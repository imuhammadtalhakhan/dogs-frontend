import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/show/:id',
    name: 'dog details',
    component: () => import('../views/DogDetail.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
